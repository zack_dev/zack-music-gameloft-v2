import React, { useState, useEffect, useCallback } from 'react';
import {
    Container, 
    PlayLists, 
    PageTitle, 
    PlayList, 
    Cover, 
    Info, 
    Title, 
    Count,
} from './styles';
import * as playListsActions from '../../store/actions/playLists';
import { useSelector, useDispatch } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';


const ListPlayList = props => {

    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const [isRefreshing, setIsRefreshing] = useState(false);

    const playlists = useSelector(state => state.playlists.availablePlayLists);
    const dispatch = useDispatch();

    const loadPlaylists = useCallback(async () => {
        setError(null);
        setIsRefreshing(true);
        try {
            await dispatch(playListsActions.fetchPlayLists());
        } catch (err) {
            setError(err.message);
        }
        setIsRefreshing(false);
    }, [dispatch, setIsLoading, setError]);


    useEffect(() => {
        
        setIsLoading(true);
        loadPlaylists().then(() => {            
            setIsLoading(false);
        });
    }, [dispatch, loadPlaylists]);

    const handlePlayListPress = (playlist) => {
        const { navigation } = props;
        navigation.navigate('PlayList', {playlist });
      };
    
    return (
        <Container>    
        <Spinner
          visible={isLoading}
          textContent={'Loading...'}
          
        />    
            <PlayLists
                ListHeaderComponent={() => <PageTitle>PlayLists</PageTitle>}
                data={playlists}
                keyExtractor={playlist => String(playlist.id)}
                renderItem={({ item: playlist }) => (
                   
                    <PlayList onPress={() => handlePlayListPress(playlist)}>
                    
                        <Cover source={{ uri: playlist.pictureUrl }} />
                        <Info>
                            <Title>{playlist.name}</Title>
                            <Count>{playlist.loadedSongs.length} songs</Count>
                        </Info>
                    </PlayList>
                )}
            />
        </Container>

    );

}

export default ListPlayList;