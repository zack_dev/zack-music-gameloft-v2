import React, { Component } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import * as playerActions from '../../store/actions/player';

import Icon from 'react-native-vector-icons/MaterialIcons';

import {
    Container,
    SongList,
    SongDetails,
    Background,
    Cover,
    PlayListName,
    Song,
    Artist,
    Title,
    BackButton,
} from './styles';

const PlayList = props => {

    const dispatch = useDispatch();

    const handleBack = () => {
        const { navigation } = props;
        navigation.goBack();
    };

    const handlePlay = (song) => {
        //console.log("song.playUrl==>",song.playUrl);
        dispatch(playerActions.songSelected(song));
    };

    const { navigation } = props;
    
    const playlist = navigation.getParam('playlist');
console.log(playlist.loadedSongs)
    return (
        <Container>
            <SongList
                ListHeaderComponent={() => (
                    <SongDetails>
                        <Background source={{ uri: playlist.pictureUrl }} blurRadius={5} />

                        <BackButton onPress={handleBack}>
                            <Icon name="arrow-back" size={24} color="#FFF" />
                        </BackButton>
                        <Cover source={{ uri: playlist.pictureUrl }} />
                        <PlayListName>{playlist.name}</PlayListName>
                    </SongDetails>
                )}

                data={playlist.loadedSongs}
                keyExtractor={song => String(song.id)}
                renderItem={({ item: song }) => (
                    <Song onPress={() => handlePlay(song)}>
                        <Title >
                            {song.title}
                        </Title>
                        <Artist>{song.artistName}</Artist>
                    </Song>
                )}

            />
        </Container>
    );
}

export default PlayList;
