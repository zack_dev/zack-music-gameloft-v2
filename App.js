import React from 'react';
import PlayListNavigator from './navigation/PlayListNavigator';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import playListsReducer from './store/reducers/playLists';
import playerReducer from './store/reducers/player';
import Player from './screens/Player/Player'

import { ApolloProvider } from "react-apollo";


const rootReducer = combineReducers({
  playlists: playListsReducer,
  player: playerReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));



function App() {

  return (
    <Provider store={store}>     
        <PlayListNavigator />  
        <Player/>   
    </Provider>
  );
}
export default App;