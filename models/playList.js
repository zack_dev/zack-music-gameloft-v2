class PlayList {
    constructor(id, pictureUrl, name) {
        this.id = id;
        this.pictureUrl = pictureUrl;
        this.name = name;
    }
}
export default PlayList;