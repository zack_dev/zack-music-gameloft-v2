import { SET_PLAYLISTS } from '../actions/playLists';

const initialState = {
    availablePlayLists: []
};


export default (state = initialState, action) => {
    if (action.type == SET_PLAYLISTS) {

        //console.log("action.songs ===>",action.playLists)
        return {
            availablePlayLists: action.playLists
            //availableSongs: action.loadedSongs
        };
    }
    else return state;
};