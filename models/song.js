class Song {
    constructor(id, title, artistName, pictureUrl, playUrl) {
        this.id = id;
        this.title = title;
        this.artistName = artistName;
        this.pictureUrl = pictureUrl;
        this.playUrl = playUrl;
    }
}

export default Song;
