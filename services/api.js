import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
  uri: 'https://us-central1-zack-playlist.cloudfunctions.net/graphql',
});

export default client;