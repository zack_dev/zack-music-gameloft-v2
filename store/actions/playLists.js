import PlayList from '../../models/playList';
import Song from '../../models/song';
import Client from '../../services/api';
import { gql } from "apollo-boost";

export const SET_PLAYLISTS = 'SET_PLAYLISTS';

const ZACK_PLAYLISTS = gql`
{
    playlists {
        name
        pictureUrl
        songs{
            title
            artist
            pictureUrl
            playUrl
        }
    }
}
`;

export const fetchPlayLists = () => {
    console.log("fetchPlayLists==>>")

    return async (dispatch, getState) => {
        try {
            Client.query({ query: ZACK_PLAYLISTS }
            ).then(result => {
                const resData = result.data.playlists;

                const loadedPlayLists = [];

                //console.log("resData6==>>>>", resData)
                for (const keyP in resData) {

                    const loadedSongs = [];

                    const songs = resData[keyP].songs;

                    for (const keyS in songs) {
                        loadedSongs.push(
                            new Song(
                                keyS,
                                songs[keyS].title,
                                songs[keyS].artist,
                                songs[keyS].pictureUrl,
                                songs[keyS].playUrl
                            )
                        )
                    }

                    loadedPlayLists.push({
                        ...new PlayList(
                            keyP,
                            resData[keyP].pictureUrl,
                            resData[keyP].name
                        ), loadedSongs
                    }
                    );

                }
                dispatch({
                    type: SET_PLAYLISTS,
                    playLists: loadedPlayLists
                });

            }).catch(err => console.log("Error=>>>>", err));

        } catch (err) {
            console.log('err occurrredd==>>', err)
            return err
            // send to custom analytics server
            //throw err;
        }
    };
};