export const spotify = {
  SPOTIFY_CLIENT_ID: '69a1f42b0cdd44fea4bc277f5f995b1d',
  SPOTIFY_REDIRECT_URL: 'spotify-ios-zack://spotify-login-callback',
  SPOTIFY_TOKEN_REFRESH_URL: 'https://zac-music-server.herokuapp.com/refresh',
  SPOTIFY_TOKEN_SWAP_URL: 'https://zac-music-server.herokuapp.com/swap',
};
