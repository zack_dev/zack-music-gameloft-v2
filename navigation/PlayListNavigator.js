import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Splash from '../screens/Splash';
import ListPlayList from '../screens/ListPlayList/ListPlayList';
import PlayList from '../screens/PlayList/PlayList'

const AppStackNavigator = createStackNavigator(
  {

    'Splash': { screen: Splash },
    'ListPlayList': { screen: ListPlayList },
    'PlayList': { screen: PlayList }
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },

);

const PlayListNavigator = createAppContainer(AppStackNavigator);
export default PlayListNavigator;
