
import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, TextInput } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import * as playerActions from '../../store/actions/player';
import {spotify} from '../../Config';

import { 
	auth as SpotifyAuth, 
	remote as SpotifyRemote, 
	ApiScope, 
	ApiConfig,
  remote
} from 'react-native-spotify-remote';

import {
  Container,
  CoverBackground,
  SongInfo,
  Title,
  Artist,
  Controls,
  ControlButton,
  ControlIcon
} from './styles'

const Player = props => {
  const song = useSelector(state => state.player.song)
  const selected = useSelector(state => state.player.selected)
  const playing = useSelector(state => state.player.playing)
  const dispatch = useDispatch()

  const config = {
    clientID: spotify.SPOTIFY_CLIENT_ID,
    redirectURL: spotify.SPOTIFY_REDIRECT_URL,
    tokenRefreshURL: spotify.SPOTIFY_TOKEN_REFRESH_URL,
    tokenSwapURL: spotify.SPOTIFY_TOKEN_SWAP_URL,
    scopes: [ApiScope.AppRemoteControlScope]
  };


  
    async function playEpicSong(playUrl){
      try{
        const session = await SpotifyAuth.authorize(config);
        await SpotifyRemote.connect(session.token);
        await remote.playUri(playUrl);
        //await remote.seek(58000);
      }catch(err){
        console.error("Couldn't authorize with or connect to Spotify",err);
      }   
    }
 
  

  const prev = () => {

    
  }

  const next = () => {}

  const play = (playUrl) => {
    console.log(" to playEpicSong",playUrl);
    playEpicSong(playUrl);
   // async () =>  ;
    //async () => await remote.resume();
    dispatch(playerActions.play())
  }

  const pause = () => {
    async () => await remote.pause();
    dispatch(playerActions.pause())
  }

  return (
    <Container>
      {selected ? (
        <>
          <CoverBackground source={{ uri: song.pictureUrl }} />
          <SongInfo>
            <Title>{song.title}</Title>
            <Artist>{song.artistName}</Artist>
          </SongInfo>

          <Controls>
            <ControlButton onPress={prev}>
              <ControlIcon name='skip-previous' />
            </ControlButton>

            <ControlButton onPress={playing ? () => pause(song.playUrl) : () => play(song.playUrl)}>
              <ControlIcon
                name={playing ? 'pause-circle-filled' : 'play-circle-filled'}
              />
            </ControlButton>

            <ControlButton onPress={next}>
              <ControlIcon name='skip-next' />
            </ControlButton>
          </Controls>
        </>
      ) : null}
    </Container>
  )
}

export default Player
