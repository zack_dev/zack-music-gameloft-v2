import { PLAY_SONG, PLAY, PAUSE } from '../actions/player'

const initialState = {
  song: {},
  playing: false,
  selected: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case PLAY_SONG:
      const mSong = action.song
      const isPlaying = false
      return { ...state, song: mSong, playing: isPlaying, selected: true }

    case PLAY:
     
      return { ...state, playing: true }

    case PAUSE:
      
      return { ...state, playing: false }
  }

  return state
}
